﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.Schema.Models
{
    public class DeleteCustomer
    {
        public Guid Id { get; set; }
    }
}
