﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL
{
    public class NotFoundException : Exception
    {
        public NotFoundException() : base("Not found") { }
    }
}
