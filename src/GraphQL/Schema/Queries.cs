﻿using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.Schema
{
    public class Queries
    {
        [UsePaging]
        [UseFiltering]
        [UseSorting]
        public IQueryable<Customer> GetCustomers(DataContext dbContext)
        => dbContext.Customers;
    }
}
